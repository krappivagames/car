﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowTruckMovement : MonoBehaviour
{
    public float m_Speed = 12f;
    public float m_TurnSpeed = 180f;

    private string m_MovementAxisName;
    private string m_TurnAxisName;
    private Rigidbody m_Rigidbody;
    private float m_MovementInputValue;
    private float m_TurnInputValue;
    private ParticleSystem[] m_particleSystemArr;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_particleSystemArr = GetComponentsInChildren<ParticleSystem>();
    }

    private void OnEnable()
    {
        // When the tow truck is turned on, make sure it's not kinematic.
        m_Rigidbody.isKinematic = false;

        // Also reset the input values.
        m_MovementInputValue = 0f;
        m_TurnInputValue = 0f;
    }

    private void Start()
    {
        // The axes names are based on player number.
        m_MovementAxisName = "Vertical";
        m_TurnAxisName = "Horizontal";
    }

    private void OnDisable()
    {
        // When the tank is turned off, set it to kinematic so it stops moving.
        m_Rigidbody.isKinematic = true;
    }

    void Update()
    {
        // Store the value of both input axes.
        m_MovementInputValue = Input.GetAxis(m_MovementAxisName);
        m_TurnInputValue = Input.GetAxis(m_TurnAxisName);
    }

    private void FixedUpdate()
    {
        // Adjust the rigidbodies position and orientation in FixedUpdate.
        Move();
        Turn();
        DustEmission();
    }

    private void Move()
    {
        //// Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
        //Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;

        //// Apply this movement to the rigidbody's position.
        //m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
        Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;
        m_Rigidbody.AddForce(movement, ForceMode.Force);
    }

    private void Turn()
    {
        // Determine the number of degrees to be turned based on the input, speed and time between frames.
        float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;

        // Make this into a rotation in the y axis.
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

        // Apply this rotation to the rigidbody's rotation.
        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
    }

    private void DustEmission()
    {
        if (Mathf.Abs(m_TurnInputValue) > .7f)
        {
            foreach (ParticleSystem ps in m_particleSystemArr)
            {
                var pse = ps.emission;
                pse.enabled = true;
            }
        }
        else
        {
            foreach (ParticleSystem ps in m_particleSystemArr)
            {
                var pse = ps.emission;
                pse.enabled = false;
            }
        }
    }
}
